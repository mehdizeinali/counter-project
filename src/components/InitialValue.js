import React from 'react'

const InitialValue = ({setCount , count}) => {
    return (
        <div>
            <input type="number" value={count ? count : ""} onChange={(e) => setCount(e.target.value)}  placeholder="initialValue"/>
        </div>
    )
}

export default InitialValue
