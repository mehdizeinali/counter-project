import React from 'react'

const Interval = ({setCountInterval,countInterval}) => {
    return (
        <div>
            <input type="number" onChange={(e) => setCountInterval(e.target.value)} value={countInterval} placeholder="interval"/>
        </div>
    )
}

export default Interval
