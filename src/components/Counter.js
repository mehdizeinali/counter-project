import React from 'react'

const Counter = ({count}) => {
    return (
        <div style={{height:'50px'}}>
            {count ? count : "unknown initial count"}
        </div>
    )
}

export default Counter
