import React from 'react'

const CounterControlBtns = ({setIsRuning, setCount, count , isRunnig,countInterval,setCountInterval}) => {
    const handleRunning=() => {
        if(!countInterval && !count){
            alert("please set values of interval and initial")
        }else if(!countInterval){
            alert("please set value of interval")
        }else if(!count){
            alert("please set value of initial count")
        }
        else{
            setIsRuning(prevState => !prevState)
        }
    }
    return (
        <div>
            <input type="button" value={isRunnig ? "pause":"play"} onClick={handleRunning}/>
            <input type="button" value="stop" onClick={() => {setCount("");setCountInterval("");setIsRuning(false)}}/>
        </div>
    )
}

export default CounterControlBtns
