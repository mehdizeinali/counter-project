import React,{useState,useEffect} from 'react';
import InitialValue from './components/InitialValue';
import Counter from './components/Counter';
import Interval from './components/Interval';
import Swich from './components/Swich';
import CounterControlBtns from './components/CounterControlBtns';

function App() {
  const [count, setCount] = useState();
  const [countInterval, setCountInterval] = useState();
  const [isIncremental, setIsIncremental] = useState(true);
  const [isRunnig, setIsRuning] = useState(false);

  useEffect(() => {
    if(isRunnig){
      if(isIncremental){
        var timer=setInterval(() => {
          setCount(count => +count+1);
        }, +countInterval);
  
      }else{
        timer=setInterval(() => {
          setCount(count => +count-1);
        }, +countInterval);
      }
      return () => clearInterval(timer);

    }else{
      return () => clearInterval(timer);
    }
  })
  
  

  return (
    <div >
    <Counter count={count}/>
    <InitialValue count={count} setCount={setCount}/>
    <Interval setCountInterval={setCountInterval} countInterval={countInterval}/>
    <CounterControlBtns setIsRuning={setIsRuning} setCountInterval={setCountInterval} isRunnig={isRunnig} setCount={setCount} count={count} countInterval={countInterval}/>
    <Swich setIsIncremental={setIsIncremental} isIncremental={isIncremental}/>
    </div>
  );
}

export default App;
